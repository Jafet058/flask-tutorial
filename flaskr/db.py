import sqlite3
import click
import time
from flask import current_app, g


def get_db():
    if 'db' not in g:
        #Se crea el archivo en la ruta especificada en caso de no existir
        g.db = sqlite3.connect(
            current_app.config['DATABASE'],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.db.row_factory = sqlite3.Row

    return g.db


def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()


#Initialazing database with the SQL script
def init_db():
    db = get_db()
#Se rellena el archivo antes especificado
    with current_app.open_resource('schema.sql') as f:
        db.executescript(f.read().decode('utf8'))

#Crea el comando y verifica si se mandó a llamar, en ese caso ejecuta la función
@click.command('init-db')
def init_db_command():
    """Clear the existing data and create new tables."""
    init_db()
    click.echo('Initialized the database.')
    
    
def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)