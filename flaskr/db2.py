from flask import g, current_app
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def get_connection():
    if 'db' not in g:
        current_app.config['DATABASE']
